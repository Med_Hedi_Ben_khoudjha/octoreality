import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:octoreality/core/constants/app-colors.dart';
import 'package:octoreality/view/screen/question-choix-unique.dart';
import 'package:octoreality/view/screen/tableau-de-bord.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:persistent_bottom_nav_bar/persistent_tab_view.dart';

abstract class HomeScreenController extends GetxController {
  changePage(int currentPage);
  goToQuestionScreen(BuildContext context);
}

class HomeScreemControllerImp extends HomeScreenController {
  late PersistentTabController persistentTabController;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    persistentTabController = PersistentTabController(initialIndex: 0);
  }

  int currentPage = 0;
  List<Widget> listPage = [
    TableauBord(),
    Center(
        child: Text(
      "Chat",
      style: TextStyle(fontSize: 20, color: AppColor.black),
    )),
    Center(child: Text("Alertes")),
    Center(child: Text("Profil")),
  ];
  List titlebottomappbar = ["Accueil", "Chat", "Alertes", "Profil"];
  
  

  List<PersistentBottomNavBarItem> navBarsItems = [
    PersistentBottomNavBarItem(
      
      icon: const Icon(
        FontAwesomeIcons.house,
        size: 18,
      ),
      title: ("Accueil"),
      activeColorPrimary: AppColor.green,
      inactiveColorPrimary: AppColor.grey,
    ),
    PersistentBottomNavBarItem(
      icon: const Icon(
        FontAwesomeIcons.commentDots,
        size: 18,
      ),
      title: ("Chat"),
      activeColorPrimary: AppColor.green,
      inactiveColorPrimary: AppColor.grey,
    ),
    PersistentBottomNavBarItem(
      icon: const Icon(
        FontAwesomeIcons.bell,
        size: 18,
      ),
      title: ("Alertes"),
      activeColorPrimary: AppColor.green,
      inactiveColorPrimary: AppColor.grey,
    ),
    PersistentBottomNavBarItem(
      icon: const Icon(
        FontAwesomeIcons.user,
        size: 18,
      ),
      title: ("Profil"),

      activeColorPrimary: AppColor.green,
      inactiveColorPrimary: AppColor.grey,
    ),
  ];

  List data = [
    {"title": "Suivi du poids", "body": "saisissez votre poids"},
    {"title": "Exercice", "body": "connectez votre app santé"}
  ];
  @override
  changePage(int index) {
    // TODO: implement changePage
    currentPage = index;
    update();
  }

  @override
  goToQuestionScreen(context) {
    PersistentNavBarNavigator.pushNewScreen(
      context,
      screen: QuestionChoix(),
      withNavBar: true, // OPTIONAL VALUE. True by default.
      pageTransitionAnimation: PageTransitionAnimation.cupertino,
    );
  }
}
