import 'package:get/get.dart';

abstract class RemercimentScreenController extends GetxController {
  gotoHome();
}

class RemercimentScreenControllerImp extends RemercimentScreenController {
  @override
  gotoHome() {
    Get.offAllNamed("/");
  }
}
