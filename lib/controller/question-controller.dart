import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:octoreality/view/screen/remerciement.dart';
import 'package:persistent_bottom_nav_bar/persistent_tab_view.dart';

abstract class QuestionScreenController extends GetxController {
  selectedIndex(int index);
  gotoRemerciment(BuildContext context);
}

class QuestionScreenControllerImp extends QuestionScreenController {
  List<String> titleButton = ["Réponse A", "Réponse B", "Réponse C"];
  int selected = 0;

  @override
  selectedIndex(index) {
    // TODO: implement selectedIndex
    selected = index;
    update();
  }
  
  @override
  gotoRemerciment(context) {
    // TODO: implement gotoRemerciment
   PersistentNavBarNavigator.pushNewScreen(
      context,
      screen: Remerciment(),
      withNavBar: true, // OPTIONAL VALUE. True by default.
      pageTransitionAnimation: PageTransitionAnimation.cupertino,
    );
  }
}
