import 'package:get/get.dart';
import 'package:octoreality/core/constants/routes.dart';
import 'package:octoreality/view/screen/home.dart';
import 'package:octoreality/view/screen/question-choix-unique.dart';
import 'package:octoreality/view/screen/remerciement.dart';
import 'package:octoreality/view/screen/tableau-de-bord.dart';

List<GetPage<dynamic>> pages = [
  GetPage(
    name: "/",
    page: () => HomeScreen(),
  ),
  GetPage(name: AppRoutes.questionChoix, page: () => QuestionChoix()),
  GetPage(name: AppRoutes.remerciment, page: () => Remerciment()),
  GetPage(name: AppRoutes.tableauBord, page: () => TableauBord()),
];
