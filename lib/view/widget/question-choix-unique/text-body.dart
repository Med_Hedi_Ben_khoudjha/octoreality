import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/widgets.dart';
import 'package:octoreality/core/constants/app-colors.dart';

class TextBody extends StatelessWidget {
  const TextBody({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: double.infinity,
          child: Divider(
            thickness: 1,
            color: AppColor.grey.withOpacity(0.2),
          ),
        ),
        SizedBox(
          height: 30,
        ),
        Text(
          "Une question à choix \n unique",
          textAlign: TextAlign.center,
          style: TextStyle(
              color: AppColor.black, fontSize: 24, fontWeight: FontWeight.bold),
        ),
        SizedBox(
          height: 20,
        ),
        Text(
          "Une seule réponse possible !",
          textAlign: TextAlign.center,
          style: TextStyle(
            color: AppColor.black,
            fontSize: 14,
          ),
        ),
      ],
    );
  }
}
