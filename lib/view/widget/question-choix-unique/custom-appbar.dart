import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:octoreality/core/constants/app-colors.dart';
import 'package:octoreality/view/screen/home.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class CustomAppBar extends StatelessWidget {
  const CustomAppBar({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: [
        IconButton(
            onPressed: () {
              Get.to(() => HomeScreen());
            },
            icon: Icon(
              FontAwesomeIcons.chevronLeft,
              color: AppColor.green,
            )),
        Text(
          "Questionnaire",
          textAlign: TextAlign.center,
          style: TextStyle(
              color: AppColor.black, fontSize: 18, fontWeight: FontWeight.bold),
        ),
        Container(
            margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
            child: CircularPercentIndicator(
              radius: 25.0,
              lineWidth: 1.0,
              percent: 0.3,
              center: Text("5/20"),
              progressColor: Colors.green,
            )),
      ],
    );
  }
}
