import 'package:flutter/cupertino.dart';
import 'package:octoreality/core/constants/app-colors.dart';

class CustomAvatar extends StatelessWidget {
  final String body;
  const CustomAvatar({super.key, required this.body});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Align(
        alignment: Alignment.topLeft,
        child: Container(
          width: 74,
          height: 74,
          margin: EdgeInsets.symmetric(vertical: 30),
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: AppColor.green,
            shape: BoxShape.circle,
          ),
          child: Text(
            body,
            textAlign: TextAlign.center,
            style: TextStyle(
                color: AppColor.white,
                fontSize: 20,
                fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }
}
