import 'package:flutter/cupertino.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/state_manager.dart';
import 'package:octoreality/controller/home-controller.dart';
import 'package:octoreality/core/constants/app-colors.dart';

class CustomGridView extends GetView<HomeScreemControllerImp> {
  final double deviceWidth;
   CustomGridView(this.deviceWidth, {super.key});

  @override
  Widget build(BuildContext context) {
    return   Container(
        height: 200,
        width: deviceWidth,
        child: GridView.builder(
          gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 200,
            mainAxisSpacing: 9,
            crossAxisSpacing: 2,
            childAspectRatio: 0.8,
          ),
          itemCount: controller.data.length,
          itemBuilder: (context, index) {
            return Container(
                margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: AppColor.grey.withOpacity(0.05)),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "${controller.data[index]["title"]}",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                          Icon(
                            FontAwesomeIcons.arrowRight,
                            size: 18,
                            color: AppColor.primaryColor,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: 88,
                      height: 88,
                      margin: EdgeInsets.symmetric(vertical: 15),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: AppColor.white,
                        shape: BoxShape.circle,
                        border: Border.all(
                            color: AppColor.secondaryColor, width: 3),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Icon(
                            FontAwesomeIcons.plus,
                            size: 20,
                            color: AppColor.primaryColor,
                          ),
                          Text(
                            "${controller.data[index]["body"]}",
                            textAlign: TextAlign.center,
                            overflow: TextOverflow.visible,
                            style: TextStyle(
                              color: AppColor.black,
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ));
          },
        ));
  }
}
