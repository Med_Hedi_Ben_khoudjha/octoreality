import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:octoreality/core/constants/app-colors.dart';

class IncompleteProfileWidegt extends StatelessWidget {
  final double deviceWidth;

  const IncompleteProfileWidegt({super.key, required this.deviceWidth});

  @override
  Widget build(BuildContext context) {
    return   Container(
      padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
      decoration: BoxDecoration(color: AppColor.grey.withOpacity(0.05)),
      child: Row(
        children: [
          Icon(
            FontAwesomeIcons.user,
            color: AppColor.primaryColor,
            size: 18,
          ),
          SizedBox(
            width: 20,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Profil incomplet",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              Text(
                "Améliorez votre profil",
                style: TextStyle(fontSize: 12),
              ),
            ],
          ),
          SizedBox(
            width: 20,
          ),
          Expanded(
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: AppColor.primaryColor,
                elevation: 3,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0)),
                minimumSize: Size(
                    deviceWidth * 0.02, deviceWidth * 0.1), //////// HERE
              ), //////// HERE
              onPressed: () {},
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    'COMPLETER',
                    style: TextStyle(fontSize: 12),
                  ), // <-- Text
                  SizedBox(
                    width: 5,
                  ),
                  Icon(
                    // <-- Icon
                    FontAwesomeIcons.pencil,
                    size: 18.0,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}