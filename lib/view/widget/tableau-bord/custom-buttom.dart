import 'package:flutter/material.dart';
import 'package:octoreality/core/constants/app-colors.dart';

class CustomButtom extends StatelessWidget {
  final String text;
  final double width;
  final double height;
  final void Function()? onPressed;
  const CustomButtom(
      {Key? key,
      required this.text,
      this.onPressed,
      required this.width,
      required this.height})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10),
      width: width,
      height: height,
      child: MaterialButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        onPressed: onPressed,
        color: AppColor.primaryColor,
        textColor: Colors.white,
        padding: EdgeInsets.symmetric(vertical: 13),
        child: Text(
          "$text",
          style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}
