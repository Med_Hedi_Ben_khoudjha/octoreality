import 'package:flutter/material.dart';
import 'package:get/state_manager.dart';
import 'package:octoreality/controller/remerciment-controller.dart';
import 'package:octoreality/core/constants/app-colors.dart';

class CustomButtonRemerciment extends GetView<RemercimentScreenControllerImp> {
  final double deviceWidth;
  final double deviceHeight;
  final String text;
  const CustomButtonRemerciment(
      {super.key,
      required this.deviceWidth,
      required this.deviceHeight,
      required this.text});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: deviceWidth,
      height: deviceHeight * 0.08,
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: MaterialButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        onPressed: () {
          controller.gotoHome();
        },
        color: AppColor.greyLight,
        textColor: Colors.white,
        padding: EdgeInsets.symmetric(vertical: 13),
        child: Text(
          text,
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
            color: AppColor.secondaryColor,
          ),
        ),
      ),
    );
  }
}
