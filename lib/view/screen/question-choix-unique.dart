import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:octoreality/controller/question-controller.dart';
import 'package:octoreality/core/constants/app-colors.dart';
import 'package:octoreality/view/widget/question-choix-unique/custom-appbar.dart';
import 'package:octoreality/view/widget/question-choix-unique/text-body.dart';
import 'package:octoreality/view/widget/tableau-bord/custom-buttom.dart';

// ignore: must_be_immutable
class QuestionChoix extends StatelessWidget {
  double? _deviceHeight;
  double? _deviceWidth;
  QuestionChoix({super.key});

  @override
  Widget build(BuildContext context) {
    _deviceHeight = MediaQuery.of(context).size.height;
    _deviceWidth = MediaQuery.of(context).size.width;
    Get.put(QuestionScreenControllerImp());
    return SafeArea(
      child: Scaffold(
          backgroundColor: AppColor.white,
          body: GetBuilder<QuestionScreenControllerImp>(
            builder: (controller) => ListView(
              children: [
                CustomAppBar(),
                TextBody(),
                SizedBox(
                  height: 20,
                ),
                ...List.generate(
                  controller.titleButton.length,
                  (index) => Container(
                    width: _deviceWidth!,
                    height: _deviceHeight! * 0.08,
                    margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    child: MaterialButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      onPressed: () {
                        controller.selectedIndex(index);
                      },
                      color: controller.selected == index
                          ? AppColor.secondaryColor
                          : AppColor.greyLight,
                      textColor: Colors.white,
                      padding: EdgeInsets.symmetric(vertical: 13),
                      child: Text(
                        " ${controller.titleButton[index]}",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: controller.selected == index
                              ? Colors.grey[350]
                              : AppColor.secondaryColor,
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: _deviceHeight! * 0.13,
                ),
                CustomButtom(
                    onPressed: () {
                      controller.gotoRemerciment(context);
                    },
                    text: "Suivant",
                    width: _deviceWidth!,
                    height: _deviceHeight! * 0.08),
              ],
            ),
          )),
    );
  }
}
