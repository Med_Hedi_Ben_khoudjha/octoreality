import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:octoreality/controller/home-controller.dart';
import 'package:octoreality/core/constants/app-colors.dart';
import 'package:persistent_bottom_nav_bar/persistent_tab_view.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(HomeScreemControllerImp());

    return GetBuilder<HomeScreemControllerImp>(
      builder: (controller) {
        return Scaffold(
          backgroundColor: AppColor.white,
          body: PersistentTabView(
            context,
            controller: controller.persistentTabController,
            screens: controller.listPage,
            items: controller.navBarsItems,
            navBarStyle: NavBarStyle.style6,
            
            confineInSafeArea: true,
            resizeToAvoidBottomInset: true,
          ),
        );
      },
    );
  }
}
