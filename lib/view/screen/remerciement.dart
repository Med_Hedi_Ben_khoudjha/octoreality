import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:octoreality/controller/remerciment-controller.dart';
import 'package:octoreality/core/constants/app-colors.dart';
import 'package:octoreality/view/widget/remerciment/custom-button.dart';

// ignore: must_be_immutable
class Remerciment extends StatelessWidget {
  double? _deviceHeight;
  double? _deviceWidth;
  Remerciment({super.key});

  @override
  Widget build(BuildContext context) {
    _deviceHeight = MediaQuery.of(context).size.height;
    _deviceWidth = MediaQuery.of(context).size.width;
    RemercimentScreenControllerImp controllerImp =
        Get.put(RemercimentScreenControllerImp());

    return SafeArea(
      child: Scaffold(
          backgroundColor: AppColor.white,
          body: SizedBox(
            height: double.infinity,
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                  child: Text(
                    "Merci pour votre réponse",
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                  ),
                ),
                CustomButtonRemerciment(
                    deviceWidth: _deviceWidth!,
                    deviceHeight: _deviceHeight!,
                    text: "Retour au tableau de bord")
              ],
            ),
          )),
    );
  }
}
