import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:octoreality/controller/home-controller.dart';
import 'package:octoreality/core/constants/app-colors.dart';
import 'package:octoreality/core/constants/images-assets.dart';
import 'package:octoreality/view/widget/tableau-bord/custom-avatar.dart';
import 'package:octoreality/view/widget/tableau-bord/custom-buttom.dart';
import 'package:octoreality/view/widget/tableau-bord/custom-gridView.dart';
import 'package:octoreality/view/widget/tableau-bord/incompete-profile-widget.dart';

class TableauBord extends StatelessWidget {
  double? _deviceHeight;
  double? _deviceWidth;

  TableauBord({super.key});

  @override
  Widget build(BuildContext context) {
    _deviceHeight = MediaQuery.of(context).size.height;
    _deviceWidth = MediaQuery.of(context).size.width;
    HomeScreemControllerImp controller = Get.put(HomeScreemControllerImp());

    return SafeArea(
        child: Scaffold(
      backgroundColor: AppColor.white,
      body: ListView(
        children: [
          Column(
            children: [
              CustomAvatar(body: "FBT"),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 15),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    "👋 Hello Foulen Ben Foulen",
                    style: TextStyle(
                        fontSize: 24,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 15),
                child: Align(
                  alignment: Alignment.center,
                  child: Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.all(10),
                    child: Text(
                      "C’est le moment de nous en dire plus sur vous 😇",
                      style: TextStyle(
                          fontSize: 44,
                          color: AppColor.black,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w100),
                    ),
                  ),
                ),
              ),
              Image.asset(
                AppAssets.questionnaire,
                height: _deviceHeight! * 0.553,
                width: MediaQuery.of(context).size.width,
                fit: BoxFit.fill,
              ),
              CustomButtom(
                  onPressed: () {
                    controller.goToQuestionScreen(context);
                  },
                  text: "Commencer !",
                  width: _deviceWidth!,
                  height: _deviceHeight! * 0.08),
              SizedBox(
                height: 15,
              ),
              IncompleteProfileWidegt(deviceWidth: _deviceWidth!),
              SizedBox(
                height: 20,
              ),
              CustomGridView(_deviceWidth!)
            ],
          )
        ],
      ),
    ));
  }
}
