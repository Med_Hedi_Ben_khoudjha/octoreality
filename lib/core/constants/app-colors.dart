import 'package:flutter/material.dart';

class AppColor {
  static const Color grey = Color(0xffafafaf);
  static const Color green = Color(0xff3DE3A5);
  static const Color black = Color(0xff000000);
  static const Color white = Color(0xffffffff);
  static const Color primaryColor = Color(0xffFF598B);
  static const Color secondaryColor = Color(0xff2E2474);
  static const Color greyLight = Color(0xffe8e8e8);

  
}
